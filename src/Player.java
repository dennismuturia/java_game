public class Player {
    String name;
    String designation;

    boolean turn;
    int count = 0;

    public Player(String name, String designation, boolean turn){
        this.name = name;
        this.designation = designation;
        this.turn = turn;
    }

    public String getName(){
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }
}
