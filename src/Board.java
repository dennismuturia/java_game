import java.util.*;

public class Board {
    Map<Integer, String> positions = new HashMap<>();
    public String[][] board = new String[3][3];
    public Board(){
        positions.put(1, "0,0");
        positions.put(2, "0,1");
        positions.put(3, "0,2");
        positions.put(4, "1,0");
        positions.put(5, "1,1");
        positions.put(6, "1,2");
        positions.put(7, "2,0");
        positions.put(8, "2,1");
        positions.put(9, "2,2");

        for (int i = 0; i < board.length; i++) {
         Arrays.fill(board[i], "1");
        }
    }


    public boolean play(Player player){
        Scanner sc = new Scanner(System.in);
        Set<Integer> keys = positions.keySet();
        int pos = 0;
        Random rd = new Random();
        boolean br = true;
        if(!player.name.equals("Computer")){
            while(br){
                System.out.println("Enter the position to play form 1 to 9:");
                pos = sc.nextInt();
                if(keys.contains(pos)){
                    br = false;
                }else{
                    System.out.println("That number is already entered. Enter another number: ");
                }
            }
        }else{
            while(br){
                pos = rd.nextInt(1, 9);
                if(keys.contains(pos)){
                    br = false;
                }else{
                    System.out.println("That number is already entered. Enter another number: ");
                }
            }
        }
            if(keys.contains(pos)){
                int x = Integer.parseInt(positions.get(pos).split(",")[0]);
                int y = Integer.parseInt(positions.get(pos).split(",")[1]);

                board[x][y] =player.designation;
                printBoard();
                String vals = positions.get(pos);
                keys.remove(pos);
                int playerCount = player.getCount() + 1;
                player.setCount(playerCount);
                System.out.println(player.getCount());
                if(player.getCount()>= 3){
                    //Get to the end of the player section
                    if(checkWon(player)){
                        System.out.println(player.name +" has won the game");
                        return true;
                    }
                }

            }
            return false;
    }

    private boolean checkWon(Player player) {
        // We need to traverse the board and check if there is a winner.
        //Start from the edges
        int x = 0, y = 0, lastPositionX = Integer.MIN_VALUE, lastPositionY = Integer.MIN_VALUE, count = 0;
        boolean isRight = true, isDown = false, isLeft = false, isUp = false;
        while(isRight || isDown || isLeft || isUp){
            if(isRight){
                if(player.designation.equals(board[x][y]) ){
                     //start following
                    if(board[x][y + 1].equals(player.designation)){
                        int i = y;
                        while(i < board[0].length){
                            if(!board[x][y].equals(player.designation)){
                                count = 0;
                                break;
                            }else{
                                count +=1;
                            }
                            if(count==3){
                                return true;
                            }
                            i++;
                            y++;
                        }

                        //Check the middle

                    }
                    if(y == board[0].length/2 + 1 && board[x][y].equals(player.designation)){
                        int i = x;
                        while(i < board.length){
                            if(!board[i][y].equals(player.designation)){
                                count = 0;
                                break;
                            }else{
                                count +=1;
                            }
                            if(count==3){
                                return true;
                            }
                            i++;
                        }
                    }

                }
                if(y < board[x].length){
                    y++;
                    if(y == board[x].length -1){
                        isDown = true;
                        isRight = false;
                    }
                }
            }

            if(isDown){
                 if(player.designation.equals(board[x][y])){
                     //start following
                     if(board[x+1][y].equals(player.designation)){
                         int i = x;
                         while(i < board[0].length){
                             if(!board[x][y].equals(player.designation)){
                                 count = 0;
                                 break;
                             }else{
                                 count +=1;
                             }
                             if(count==3){
                                 return true;
                             }
                             x++;
                             i++;
                         }

                     }
                     if(x == board.length/2 + 1 && board[x][y].equals(player.designation)){
                         int i = y;
                         while(i >= 0){
                             if(!board[x][i].equals(player.designation)){
                                 count = 0;
                                 break;
                             }else{
                                 count +=1;
                             }
                             if(count==3){
                                 return true;
                             }
                             i--;
                         }
                     }
                 }
                 if(x < board.length){
                     x++;
                     if(x == board.length -1){
                         isDown = false;
                         isLeft = true;
                     }
                 }
            }

            if(isLeft){
                if(player.designation.equals(board[x][y])){
                    //Check diagonally
                    int p = x, r = y;
                    while(p >= 0 && r >= 0){
                        if(!board[p][r].equals(player.designation)){
                            count =0;
                            break;
                        }else{
                            count +=1;
                        }
                        if(count==3){
                            return true;
                        }
                        p --;
                        r --;
                    }

                    //Start following
                    if(board[x][y - 1].equals(player.designation)){
                        int i = y;
                        while(i >= 0){
                            if(!board[x][y].equals(player.designation)){
                                count = 0;
                                break;
                            }else{
                                count +=1;
                            }
                            if(count==3){
                                return true;
                            }
                            i--;
                            y--;
                        }
                    }
                }
                if(y > -1){
                    y--;
                    if(y == 0){
                        isLeft = false;
                        isUp = true;
                    }

                }
            }

            if(isUp){
                if(player.designation.equals(board[x][y])){
                    //Check diagonally
                    int p = x, r = y;
                    while(p >= 0 && r < board[x].length){
                        if(!board[p][r].equals(player.designation)){
                            count = 0;
                            break;
                        }else{
                            count +=1;
                        }
                        if(count==3){
                            return true;
                        }
                        p --;
                        r ++;
                    }

                    //Start following
                    if(board[x - 1][y].equals(player.designation)){
                        int i = x;
                        while(i >= 0){
                            if(!board[x][y].equals(player.designation)){
                                count = 0;
                                break;
                            }else{
                                count +=1;
                            }
                            if(count==3){
                                return true;
                            }
                            i--;
                            x--;
                        }

                    }
                }
                if(x > -1){
                    x--;
                    if(x == 0){
                        isUp = false;
                    }
                }
            }
        }
        return false;
    }

    public void printBoard(){
        for (String[] ints : board) {
            System.out.print("[");
            for (int j = 0; j < ints.length; j++) {
                System.out.print(ints[j]);
                if (j != ints.length - 1) {
                    System.out.print(",");
                }
            }
            System.out.print("]");
            System.out.println();
        }
    }
}
