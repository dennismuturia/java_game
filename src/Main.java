import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to ping pong game");
        Board board = new Board();
        board.printBoard();

        //There will be 2 players. Player one will be human, player 2  will be computer;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your name:");
        String playerName = sc.next();
        Player player1 = new Player(playerName, "X", true);
        Player player2 = new Player("Computer", "O", false);


        //Play time
        boolean br = false;
        boolean br2 = false;
        while(!br&&!br2){
            br = board.play(player1);
            if(br) break;
            br2 = board.play(player2);
            if(br2) break;
        }

    }



}
